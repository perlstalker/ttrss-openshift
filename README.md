# TT-RSS on OpenShift.

This is based heavily on https://github.com/disconn3ct/tiny_tiny_rss-openshift-quickstart.

The main differences are:

* Sphinx is not enabled.
* Uses mysql rather than pgsql.

# Setup

In theory this works.

     rhc app create ttrss php-5.4 mysql-5.5 cron-1.4 --from-code=https://gitlab.com/perlstalker/ttrss-openshift.git --timeout=9999

The above should work but it always times out for me. To work around
that, create the app first then import the repo by hand.

     rhc app create ttrss php-5.4 mysql-5.5 cron-1.4
     git remote add ttrss-openshift https://gitlab.com/perlstalker/ttrss-openshift.git
     git pull -s recursive -X theirs ttrss-openshift master
	 git submodule update --init php
     git add -A
     git commit -m 'initial deployment'
	 git push origin
 
I had problems getting mysql to connect to the DB in post_deploy. I can import the schema by hand.

    mysql ttrss < $OPENSHIFT_REPO_DIR/php/schema/ttrss_schema_mysql.sql
	touch $OPENSHIFT_DATA_DIR/.schema_deployed

# Updating TTRSS

The first time, you have to set it to master:

    cd php
    git checkout master

After that, all that is necessary is:

    cd php
    git pull
    cd ..
    git add php
    git commit -m "update to lastest ttrss"
    git push

